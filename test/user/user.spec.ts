var chai = require('chai');
chai.should();
chai.use(require('chai-things'));
import app from '../../src/app';

import * as request from 'supertest';
import * as rewire from 'rewire';
import * as sinon from 'sinon';
import { UserSchema, UserModel } from '../../src/models/user';

const userCredentials = {
  email: 'prueba25@mail.com',
  password: '123456'
};

let token = '';
let userId = '';

let authenticatedUser = request.agent(app);

before(function(done) {
  authenticatedUser
    .post('/login')
    .send(userCredentials)
    .expect(200)
    .end(function(err, res) {
      token = res.body.token;
      userId = res.body.usuario._id;
      done();
    });
});

describe('POST /newUser', () => {
  it('Should create a new user', done => {
    request(app)
      .post('/newUser')
      .send({
        firstName: 'Dani',
        lastName: 'Lopez',
        password: '123123',
        email: 'mailmail@mail.com'
      })
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        UserModel.find().then(users => {
          users.should.contain.a.thing.with.property('email', 'mailmail@mail.com');
          done();
        });
      });
  });
});

describe('PUT /modifyUser', () => {
  it('Should modify a new user', done => {
    let timestamp = new Date().getTime().toString();
    request(app)
      .put('/modifyUser')
      .set({ token: token })
      .send({
        firstName: 'Test',
        lastName: timestamp,
        id: userId
      })
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        UserModel.find().then(users => {
          users.should.contain.a.thing.with.property('lastName', timestamp);
          users.should.contain.a.thing.with.property('firstName', 'Test');
          done();
        });
      });
  });
});
