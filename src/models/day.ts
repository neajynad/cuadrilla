import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const DaySchema = new Schema({
  date: {
    type: Date,
    required: 'Enter a date',
    unique: true
  },
  observations: {
    type: String
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
});

export const DayModel = mongoose.model('Day', DaySchema);
