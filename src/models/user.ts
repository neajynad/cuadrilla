import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const UserSchema = new Schema({
  email: {
    type: String,
    required: 'Enter a mail',
    trim: true,
    unique: true
  },
  firstName: {
    type: String,
    required: 'firstName is required'
  },
  lastName: {
    type: String,
    required: 'lastName is required'
  },
  password: {
    type: String,
    required: 'password is required'
  }
});

interface IUserModel extends mongoose.Document {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
}

export const UserModel = mongoose.model<IUserModel>('User', UserSchema);
