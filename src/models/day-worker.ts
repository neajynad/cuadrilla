import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const DayWorkerSchema = new Schema({
  date: {
    type: Schema.Types.ObjectId,
    ref: 'Day'
  },
  worker: {
    type: Schema.Types.ObjectId,
    ref: 'Worker'
  }
});

export const DayWorkerModel = mongoose.model('DayWorker', DayWorkerSchema);
