import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const WorkerSchema = new Schema({
  firstName: {
    type: String,
    required: 'firstName is required',
    trim: true
  },
  lastName: {
    type: String,
    required: 'lastName is required',
    trim: true
  },
  moneyOnAccount: {
    type: Number
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
});

export const WorkerModel = mongoose.model('Worker', WorkerSchema);
