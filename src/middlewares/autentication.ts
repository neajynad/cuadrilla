import * as jwt from 'jsonwebtoken';
import {SEED} from "../config/config";

//To verify token
export function verifyToken(req, res, next) {
    var token = req.get('token');
    jwt.verify(token, SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                mensaje: 'Token incorrecto',
                errors: err
            });
        }
        req.usuario = decoded.usuario;
        next();
    });
}