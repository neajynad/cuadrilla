import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import { Routes } from './routes/routes';
import { LoginRoutes } from './routes/usuarioRoutes';
import { WorkerRoutes } from './routes/workerRoutes';

class App {
  public app: express.Application;
  public routes: Routes = new Routes();
  public loginRoutes: LoginRoutes = new LoginRoutes();
  public workerRoutes: WorkerRoutes = new WorkerRoutes();

  constructor() {
    this.app = express();
    this.config();

    //Definimos las rutas
    this.routes.routes(this.app);
    this.loginRoutes.routes(this.app);
    this.workerRoutes.routes(this.app);
  }

  private config(): void {
    //Conexión a la DB
    mongoose.connect(
      'mongodb://localhost:27017/cuadrilla',
      (err, res) => {
        if (err) throw err;
        console.log('Base de datos online');
      },
      { useNewUrlParser: true }
    );

    // support application/json type post data
    this.app.use(bodyParser.json());
    //support application/x-www-form-urlencoded post data
    this.app.use(bodyParser.urlencoded({ extended: false }));
  }
}

export default new App().app;
