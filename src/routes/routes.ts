import { Request, Response } from 'express';
import { verifyToken } from '../middlewares/autentication';

export class Routes {
  users = [
    {
      name: 'Daniel',
      lastName: 'Lopez'
    },
    {
      name: 'Almudena',
      lastName: 'Linares'
    },
    {
      name: 'Juanfri',
      lastName: 'Lopez'
    }
  ];

  public routes(app): void {
    app.route('/').get((req: Request, res: Response) => {
      res.status(200).send({
        error: 'Page not found'
      });
    });

    app.route('/getUsersTest').get((req: Request, res: Response) => {
      res.status(200).send(this.users);
    });
  }
}
