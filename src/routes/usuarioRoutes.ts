import { UserController } from '../controllers/userController';
import { verifyToken } from '../middlewares/autentication';

export class LoginRoutes {
  public userController = new UserController();

  public routes(app): void {
    app.route('/login').post(this.userController.login);

    app.route('/newUser').post(this.userController.addNewUser);

    app.route('/modifyUser').put(verifyToken, this.userController.modifyUser);
  }
}
