import { WorkerController } from '../controllers/workerController';

export class WorkerRoutes {
  public workerController = new WorkerController();

  public routes(app): void {
    app.route('/addWorker').post(this.workerController.addNewWorker);
    app.route('/modifyWorker').put(this.workerController.modifyWorker);
  }
}
