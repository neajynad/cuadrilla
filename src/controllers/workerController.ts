import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { UserSchema } from '../models/user';
import { Request, Response } from 'express';
import { SEED } from '../config/config';
import { WorkerModel } from '../models/worker';

export class WorkerController {
  public addNewWorker(req: Request, res: Response) {
    let body = req.body;
    let newWorker = new WorkerModel({
      firstName: body.firstName,
      lastName: body.lastName,
      moneyOnAccount: body.moneyOnAccount,
      user: body.user
    });
    newWorker.save((err, user) => {
      if (err) {
        res.send(err);
      }
      res.json(user);
    });
  }

  public modifyWorker(req: Request, res: Response) {
    let body = req.body;
    let id = body.id;

    WorkerModel.findById(id, (err, worker) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          mensaje: 'Error finding worker',
          errors: err
        });
      }

      if (!worker) {
        return res.status(400).json({
          ok: false,
          mensaje: 'Worker with id ' + id + ' doesnt exist',
          errors: { message: 'Doesnt exist worker with this id' }
        });
      }

      worker.firstName = body.firstName;
      worker.lastName = body.lastName;

      worker.save((err, workerSaved) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: 'Error updating worker',
            errors: err
          });
        }

        res.status(200).json({
          ok: true,
          usuario: workerSaved
        });
      });
    });
  }

  // public login(req: Request, res: Response) {
  //   let body = req.body;

  //   User.findOne({ email: body.email }, (err, usuarioDB) => {
  //     if (err) {
  //       return res.status(500).json({
  //         ok: false,
  //         mensaje: 'Error al buscar usuario',
  //         errors: err
  //       });
  //     }
  //     if (!usuarioDB) {
  //       return res.status(400).json({
  //         ok: false,
  //         mensaje: 'Credenciales incorrectas - email',
  //         errors: err
  //       });
  //     }
  //     if (!bcrypt.compare(body.password, usuarioDB.password)) {
  //       return res.status(400).json({
  //         ok: false,
  //         mensaje: 'Credenciales incorrectas - password',
  //         errors: err
  //       });
  //     }

  //     let token = jwt.sign({ usuario: usuarioDB }, SEED, { expiresIn: '365d' });
  //     usuarioDB.password = ':)';
  //     res.status(200).json({
  //       ok: true,
  //       mensaje: 'Ha ido bien',
  //       usuario: usuarioDB,
  //       token: token,
  //       errors: err
  //     });
  //   });
  // }
}
