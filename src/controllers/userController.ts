import { UserModel } from './../models/user';
import * as mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { UserSchema } from '../models/user';
import { Request, Response } from 'express';
import { SEED } from '../config/config';

export class UserController {
  public addNewUser(req: Request, res: Response) {
    let body = req.body;
    let newUser = new UserModel({
      firstName: body.firstName,
      lastName: body.lastName,
      password: body.password ? bcrypt.hashSync(String(body.password), 10) : '',
      email: body.email
    });
    newUser.save((err, user) => {
      if (err) {
        res.send(err);
      }
      res.json(user);
    });
  }

  public modifyUser(req: Request, res: Response) {
    let body = req.body;
    let id = body.id;

    UserModel.findById(id, (err, user) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          mensaje: 'Error al buscar usuario',
          errors: err
        });
      }

      if (!user) {
        return res.status(400).json({
          ok: false,
          mensaje: 'El usuario con el id ' + id + ' no existe',
          errors: { message: 'No existe un usuario con ese ID' }
        });
      }

      user.firstName = body.firstName;
      user.lastName = body.lastName;

      user.save((err, usuarioGuardado) => {
        if (err) {
          return res.status(400).json({
            ok: false,
            mensaje: 'Error al actualizar usuario',
            errors: err
          });
        }
        usuarioGuardado.password = ':)';

        res.status(200).json({
          ok: true,
          usuario: usuarioGuardado
        });
      });
    });
  }

  public login(req: Request, res: Response) {
    let body = req.body;

    UserModel.findOne({ email: body.email }, (err, usuarioDB) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          mensaje: 'Error al buscar usuario',
          errors: err
        });
      }
      if (!usuarioDB) {
        return res.status(400).json({
          ok: false,
          mensaje: 'Credenciales incorrectas - email',
          errors: err
        });
      }
      if (!bcrypt.compare(body.password, usuarioDB.password)) {
        return res.status(400).json({
          ok: false,
          mensaje: 'Credenciales incorrectas - password',
          errors: err
        });
      }

      let token = jwt.sign({ usuario: usuarioDB }, SEED, { expiresIn: '365d' });
      usuarioDB.password = ':)';
      res.status(200).json({
        ok: true,
        mensaje: 'Ha ido bien',
        usuario: usuarioDB,
        token: token,
        errors: err
      });
    });
  }
}
